#include "threadpool.hpp"
#include "filestat.hpp"
#include <iostream>

using std::filesystem::recursive_directory_iterator;
using std::filesystem::path;

int main(int argc, char*argv[]) {

    if(argc < 2) {

        std::cerr<<"insufficient number of arguments: Usage: ./FileAnalyzer path_to_directory";
        exit(1);
    }

    std::string path_to_dir = argv[1];
    size_t number_of_files = 0;
    std::vector<std::shared_ptr<std::ifstream>> streams;
    std::vector<std::shared_ptr<FileStat>> results;
{
    ThreadPool pool(5);


    for(const auto& file : recursive_directory_iterator(path_to_dir)) {

        if(std::filesystem::is_regular_file(file)) {
             
            number_of_files++;

            std::ifstream file_stream(file.path().string());
                

            if(!file_stream.is_open()) {
                std::cerr<<"The stream to "<<file<<" could not be opened\n";
                continue;
            }

            auto stream = std::make_shared<std::ifstream>(std::move(file_stream));
            auto result = std::make_shared<FileStat>();

            result->path = std::filesystem::absolute(file.path());
            pool.submit(&FileStat::analyze,result,stream);
            
            results.emplace_back(result);                
            streams.emplace_back(stream);
        }

    }
}
    std::cout<<"Total number of files in "<< path_to_dir << " is "<< number_of_files << std::endl;

    for(auto& s : streams) {

        s->close();
    }

    for(const auto& res : results){

        std::cout << *res<<std::endl;
    }

    return 0;
}
