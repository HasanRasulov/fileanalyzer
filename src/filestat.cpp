#include "filestat.hpp"

FileStat::FileStat():total_lines{0},empty_lines{0},non_empty_lines{0},words{0},letters{0}{}

std::ostream& operator<<(std::ostream& os,const FileStat& obj) {

    os << obj.path << " {\n";
    os << "Total number of lines: " << obj.total_lines << "\nNumber of empty lines: " << obj.empty_lines << "\nNumber of non-empty lines: " << obj.non_empty_lines <<"\nTotal number of words:"<<obj.words<< "\nTotal number of letters: " << obj.letters;
    os <<"\n}" << std::endl;
    return os;
}


void FileStat::count_words(std::string& line) {

    if(line.empty())  return;

    for(size_t i=0; i<line.length() - 1; i++) {

        if(isspace(line[i]) ||line[i] == '\t' || line[i] == '\n'  && isalpha(line[i+1])) this->words++;

    }

    if(!isspace(line[0])) this->words++;

}

void FileStat::analyze(std::shared_ptr<std::ifstream> stream) {

    std::string line;
    while(std::getline(*stream,line)) {

        this->total_lines++;

        if(line.empty()) {
            empty_lines++;
            continue;
        }

        this->letters += std::count_if(line.begin(),line.end(),isalpha);

        count_words(line);

    }

    this->non_empty_lines = this->total_lines - this->empty_lines;
}

