#include "threadpool.hpp"

ThreadPool::ThreadPool(size_t num):workers_num{num} {

    running = false;

    for(size_t i = 0; i < workers_num; i++ ) {

        workers.emplace_back([=] {

            while(true) {


                std::packaged_task<void()> task;
                {
                    std::unique_lock<std::mutex> lock{idle_worker_lock};

                    idle_worker.wait(lock, [=] { return running || !tasks.empty(); });

                    if (running && tasks.empty())
                        break;

                    task = std::move(*tasks.front());
                    tasks.pop();

                }

                task();


            }


        });

    }

}


ThreadPool::~ThreadPool() {

    running = true;

    idle_worker.notify_all();

    for (auto &worker : workers)
        if(worker.joinable())
            worker.join();

}

void ThreadPool::submit(std::function<Task_type> task,std::shared_ptr<FileStat> obj,std::shared_ptr<std::ifstream> stream) {  

    auto task_ptr = std::make_shared<std::packaged_task<void()>>(std::bind(task,obj,stream));

    std::unique_lock<std::mutex> lock{idle_worker_lock};

    tasks.push(task_ptr);

    idle_worker.notify_one();

}
