#include<gtest/gtest.h>

#include "../include/filestat.hpp"

TEST(COUNTWORDS,EMPTY){

   FileStat fs;
   std::string line = "";
   fs.count_words(line);
   ASSERT_EQ(fs.words,0); 

}

TEST(COUNTWORDS,TEST1){

   FileStat fs;
   std::string line = "Hasan";
   fs.count_words(line);
   ASSERT_EQ(fs.words,1);

}
TEST(COUNTWORDS,TEST2){

   FileStat fs;
   std::string line = "Hasan\nRasulov";
   fs.count_words(line);
   ASSERT_EQ(fs.words,2);

}
TEST(COUNTWORDS,TEST3){

   FileStat fs;
   std::string line = "Hasan\tRasulov";
   fs.count_words(line);
   ASSERT_EQ(fs.words,2);

}
TEST(COUNTWORDS,TEST4){

   FileStat fs;
   std::string line = " ";
   fs.count_words(line);
   ASSERT_EQ(fs.words,0);

}

TEST(COUNTWORDS,TEST5){

   FileStat fs;
   std::string line = " Hasan Rasulov ";
   fs.count_words(line);
   ASSERT_EQ(fs.words,2);

}
TEST(COUNTWORDS,TEST6){

   FileStat fs;
   std::string line = "\tMy name is Hasan.How about you?\n";
   fs.count_words(line);
   ASSERT_EQ(fs.words,6);

}
TEST(COUNTWORDS,TEST7){

   FileStat fs;
   std::string line = "Hasan #123 132";
   fs.count_words(line);
   ASSERT_EQ(fs.words,3);

}
TEST(COUNTWORDS,TEST8){

   FileStat fs;
   std::string line = "       H        ";
   fs.count_words(line);
   ASSERT_EQ(fs.words,1);

}
TEST(COUNTWORDS,TEST9){

   FileStat fs;
   std::string line = "~/FileAnalyzer/build";
   fs.count_words(line);
   ASSERT_EQ(fs.words,1);

}
TEST(COUNTWORDS,TEST10){

   FileStat fs;
   std::string line = "What is ur name ?!";
   fs.count_words(line);
   ASSERT_EQ(fs.words,5);

}

TEST(ANALYZE,TEST1){
    
   FileStat fs;
   std::ifstream stream("../test/dirs/file");
   
   fs.analyze(std::make_shared<std::ifstream>(std::move(stream)));
   
   ASSERT_EQ(fs.total_lines,11);
   ASSERT_EQ(fs.empty_lines,6);
   ASSERT_EQ(fs.non_empty_lines,5);
   ASSERT_EQ(fs.words,62);
   ASSERT_EQ(fs.letters,44);    

   stream.close(); 

}

TEST(ANALYZE,TEST2){

   FileStat fs;
   std::ifstream stream("../test/dirs/dir1/file1");
   
   fs.analyze(std::make_shared<std::ifstream>(std::move(stream)));
   
   ASSERT_EQ(fs.total_lines,19);
   ASSERT_EQ(fs.empty_lines,5);
   ASSERT_EQ(fs.non_empty_lines,14);
   ASSERT_EQ(fs.words,57);
   ASSERT_EQ(fs.letters,273);

   stream.close(); 

}
TEST(ANALYZE,TEST3){

   FileStat fs;
   std::ifstream stream("../test/dirs/dir1/file2");
   
   fs.analyze(std::make_shared<std::ifstream>(std::move(stream)));
   
   ASSERT_EQ(fs.total_lines,15);
   ASSERT_EQ(fs.empty_lines,0);
   ASSERT_EQ(fs.non_empty_lines,15);
   ASSERT_EQ(fs.words,53);
   ASSERT_EQ(fs.letters,263);

   stream.close(); 

}


TEST(ANALYZE,TEST4){

   FileStat fs;
   std::ifstream stream("../test/dirs/dir1/file");
   
   fs.analyze(std::make_shared<std::ifstream>(std::move(stream)));
   
   ASSERT_EQ(fs.total_lines,11);
   ASSERT_EQ(fs.empty_lines,6);
   ASSERT_EQ(fs.non_empty_lines,5);
   ASSERT_EQ(fs.words,62);
   ASSERT_EQ(fs.letters,44);

   stream.close(); 

}

TEST(ANALYZE,TEST5){

   FileStat fs;
   std::ifstream stream("../test/dirs/dir1/dir2/file123");
   
   fs.analyze(std::make_shared<std::ifstream>(std::move(stream)));
   
   ASSERT_EQ(fs.total_lines,11);
   ASSERT_EQ(fs.empty_lines,6);
   ASSERT_EQ(fs.non_empty_lines,5);
   ASSERT_EQ(fs.words,62);
   ASSERT_EQ(fs.letters,44);

   stream.close(); 

}

TEST(ANALYZE,TEST6){

   FileStat fs;
   std::ifstream stream("../test/dirs/dir1/dir2/file");
   
   fs.analyze(std::make_shared<std::ifstream>(std::move(stream)));
   
   ASSERT_EQ(fs.total_lines,11);
   ASSERT_EQ(fs.empty_lines,6);
   ASSERT_EQ(fs.non_empty_lines,5);
   ASSERT_EQ(fs.words,62);
   ASSERT_EQ(fs.letters,44);

   stream.close(); 

}


