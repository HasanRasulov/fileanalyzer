#ifndef __THREADPOOL__HPP__
#define __THREADPOOL__HPP__

#include <queue>
#include <thread>
#include <atomic>
#include <condition_variable>
#include <future>
#include <fstream>
#include <vector>
#include <functional>
#include <utility>
#include <memory>

#include "filestat.hpp"

class ThreadPool{

  public:
   using Task_type = void(std::shared_ptr<FileStat>,std::shared_ptr<std::ifstream>);
  private:
   size_t workers_num;

   std::atomic<bool> running;
   std::vector<std::thread> workers;
   std::queue<std::shared_ptr<std::packaged_task<void()>>> tasks;
   std::mutex idle_worker_lock;
   std::condition_variable idle_worker;

  public:
    ThreadPool(size_t);
    ~ThreadPool(); 
    void submit(std::function<Task_type>,std::shared_ptr<FileStat>,std::shared_ptr<std::ifstream>);
        



};


#endif
