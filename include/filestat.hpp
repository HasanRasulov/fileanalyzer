#ifndef __FILESTAT__HPP__
#define __FILESTAT__HPP__

#include <fstream>
#include <cctype>
#include <filesystem>
#include <algorithm>
#include <ostream>
#include <memory>
#include <gtest/gtest.h>
struct FileStat{

  std::filesystem::path path;
  
  size_t total_lines;
  size_t empty_lines;
  size_t non_empty_lines;
  size_t words;
  size_t letters;
  
  friend std::ostream& operator<<(std::ostream&,const FileStat&);
  FileStat(); 
 
  void analyze(std::shared_ptr<std::ifstream>);
  
  private:
    FRIEND_TEST(COUNTWORDS,TEST1);  
    FRIEND_TEST(COUNTWORDS,TEST2);  
    FRIEND_TEST(COUNTWORDS,TEST3);  
    FRIEND_TEST(COUNTWORDS,TEST4);  
    FRIEND_TEST(COUNTWORDS,TEST5);  
    FRIEND_TEST(COUNTWORDS,TEST6);  
    FRIEND_TEST(COUNTWORDS,TEST7);  
    FRIEND_TEST(COUNTWORDS,TEST8);  
    FRIEND_TEST(COUNTWORDS,TEST9);  
    FRIEND_TEST(COUNTWORDS,TEST10);  
    FRIEND_TEST(COUNTWORDS,EMPTY);
    void count_words(std::string&);
};


#endif
